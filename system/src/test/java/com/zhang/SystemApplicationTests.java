package com.zhang;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.mapper.*;
import com.zhang.pojo.*;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
class SystemApplicationTests {
    @Autowired
	private AccountMapper accountMapper;
	@Autowired
	private ActivityMapper activityMapper;
	@Autowired
	private MemberMapper memberMapper;

	@Autowired
	private OrganMapper organMapper;
	@Test
	void contextLoads() {
	}


	@Test
	public void  getAllDepartment() {
		Page<Organ> page = new Page<>(2,2);
		QueryWrapper<Organ> wrapper = new QueryWrapper<>();
		IPage<Organ> iPage = organMapper.selectPage(page,null);
		iPage.getRecords().forEach(System.out::println);

	}
	@Test
	public void DepartmentById(){
		QueryWrapper<Organ> wrapper = new QueryWrapper<>();
		wrapper.select("id","name");
		List<Organ> l = organMapper.selectList(wrapper);
		l.forEach(System.out::println);
	}
	@Test
	public void selectActById() {
		Account account = new Account();
		account.setId(1);
		account.setCreateTime(new Date());
		account.setAccount("abc");
		account.setEmail("123");
		account.setSex(1);
		account.setPhone("1111123");
		account.setModifyTime(new Date());
		account.setOid("篮球社");
		account.setPassword("123456");
		account.setName("123");
		accountMapper.updateById(account);
	}
	@Test
	public void page(){
		Page<Member> page = new Page<>(1, 5);
		IPage<Member> iPage = memberMapper.selectPage(page, null);
		System.out.println(iPage.getRecords());


	}


}
