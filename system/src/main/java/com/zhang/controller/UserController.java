package com.zhang.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Account;
import com.zhang.pojo.Organ;
import com.zhang.service.AccountService;
import com.zhang.service.OragnService;
import com.zhang.util.Result;
import com.zhang.vo.AccountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
   @Autowired
   private AccountService accountService;
   @Autowired
   private OragnService oragnService;

   @GetMapping("")
   public String toEmpListUI() {
      return "User/userlist";
   }

   @GetMapping("/userlist")
   @ResponseBody
   public Result<Object> list(AccountVo accountVo, @RequestParam("page") int page, @RequestParam("limit") int limit) {
      IPage<Account> iPage = this.accountService.getAllAccount(accountVo, page, limit);
      Long count = iPage.getTotal();
      return Result.success(iPage.getRecords(), count);
   }

   @DeleteMapping("/delete/{id}")
   @ResponseBody
   public Result<Object> DelCount(@PathVariable("id") int id) {
      this.accountService.DelAccountById(id);
      return Result.success("删除成功");
   }

   @GetMapping("/add/ui")
   public String toAddUI(Model model) {
      List<Organ> list = this.oragnService.getAllDept();
      model.addAttribute("organ", list);
      return "User/adduser";
   }

   @PostMapping("/adduser")
   @ResponseBody
   public Result<Object> addUser(Account account) {
      account.setCreateTime(new Date());
      Integer result = this.accountService.AddAccount(account);
      return result == -1 ? Result.fail("该账号已存在,无法添加") : Result.success("添加成功");
   }

   @GetMapping("/edit/{id}")
   public String toEdit(@PathVariable("id") Integer id, Model model) {
      Account account = this.accountService.getOne(id);
      List<Organ> oidlist = this.oragnService.getAllDept();
      model.addAttribute("organ", oidlist);
      model.addAttribute("account", account);
      return "User/UserEdit";
   }

   @PutMapping("/update")
   @ResponseBody
   public Result<Object> update(Account account) {
      account.setModifyTime(new Date());
      this.accountService.update(account);
      return Result.success("用户更新成功");
   }

   @PostMapping("/password/update")
   @ResponseBody
   public Result<Object> passwordupdate(HttpServletRequest request) {
      HttpSession session = request.getSession();
      Account userInfo = (Account)session.getAttribute("userInfo");
      Account account = this.accountService.getOne(userInfo.getId());
      String newpassword = request.getParameter("newpassword");
      String againpassword = request.getParameter("againpassword");
      String oldpassword = request.getParameter("oldpassword");
      if (!newpassword.equals(againpassword)) {
         return Result.fail("输入的两次密码不一致");
      } else if (!account.getPassword().equals(oldpassword)) {
         return Result.fail("输入的旧密码错误");
      } else {
         account.setPassword(newpassword);
         this.accountService.update(account);
         return Result.success("密码修改成功");
      }
   }
}
