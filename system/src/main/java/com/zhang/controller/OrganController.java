package com.zhang.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Organ;
import com.zhang.service.impl.OrganServiceImpl;
import com.zhang.util.Result;
import com.zhang.vo.OrginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/organ")
public class OrganController {
   @Autowired
   private OrganServiceImpl organService;

   @GetMapping("")
   public String or() {
      return "Community/CommuntiyList";
   }

   @GetMapping("/list")
   @ResponseBody
   public Result<Object> list(OrginVo orginVo, HttpServletRequest request, @RequestParam("page") int page, @RequestParam("limit") int limit) {
      IPage<Organ> iPage = this.organService.getAllOragn(orginVo, page, limit);
      Long count = iPage.getTotal();
      return Result.success(iPage.getRecords(), count);
   }

   @DeleteMapping("/delorgan/{org}")
   @ResponseBody
   public Result<Object> del(@PathVariable("org") String ids) {
      String[] s1 = ids.split(",");

      for(int i = 0; i < s1.length; ++i) {
         int wq = Integer.parseInt(s1[i]);
         this.organService.delete(wq);
      }

      return Result.success("删除成功");
   }

   @PostMapping("/addCom")
   @ResponseBody
   public Result<Object> add(Organ organ) {
      this.organService.addOragn(organ);
      return Result.success("添加成功");
   }

   @GetMapping("/add")
   public String toaddUI() {
      return "Community/CommunityAdd";
   }

   @GetMapping("/getOne/{id}")
   public String getOne(@PathVariable("id") Integer id, Model model) {
      Organ organ = this.organService.getOne(id);
      model.addAttribute("organ", organ);
      return "Community/CommunityEdit";
   }

   @PutMapping("/update")
   @ResponseBody
   public Result<Object> updateemp(Organ organ) {
      this.organService.Update(organ);
      return Result.success("修改成功");
   }
}
