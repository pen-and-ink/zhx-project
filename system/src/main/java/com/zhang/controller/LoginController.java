package com.zhang.controller;

import com.wf.captcha.utils.CaptchaUtil;
import com.zhang.pojo.Account;
import com.zhang.service.AccountService;
import com.zhang.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
   @Autowired
   private AccountService accountService;

   @PostMapping("/login")
   @ResponseBody
   public Result<Object> login(Account user, HttpSession session, HttpServletRequest request, @RequestParam("captcha") String captcha) {
      String account = user.getAccount();
      String password = user.getPassword();
      Account result = this.accountService.login(account, password);
      if (!CaptchaUtil.ver(captcha, request)) {
         CaptchaUtil.clear(request);
         return Result.fail("验证码不正确!");
      } else if (result != null) {
         result.setPassword((String)null);
         session.setAttribute("userInfo", result);
         session.setAttribute("role", result.getRole());
         session.setAttribute("oid", result.getOid());
         return Result.success("登录成功");
      } else {
         return Result.fail("用户名或密码错误！");
      }
   }

   @GetMapping("/index")
   public String index(HttpServletRequest request) {
      HttpSession session = request.getSession();
      Integer role = (Integer)session.getAttribute("role");
      return role == 1 ? "index" : "studentIndex";
   }
}
