package com.zhang.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Account;
import com.zhang.pojo.Activity;
import com.zhang.service.ActivityService;
import com.zhang.util.Result;
import com.zhang.vo.ActivityVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/activity")
public class ActivityController {
   @Autowired
   private ActivityService activityService;

   @GetMapping("/add/ui")
   public String toadd(Model model, HttpServletRequest request) {
      HttpSession session = request.getSession();
      Account userInfo = (Account)session.getAttribute("userInfo");
      String oid = userInfo.getOid();
      if (!oid.equals("admin")) {
         model.addAttribute("oid", oid);
      } else {
         oid = "admin";
         model.addAttribute("oid", oid);
      }

      return "Activity/ActivityAdd";
   }

   @GetMapping("/listtest")
   public String a() {
      return "Activity/List";
   }

   @GetMapping("/list")
   public String b() {
      return "Activity/ActList";
   }

   @PostMapping({"/add"})
   @ResponseBody
   public Result<Object> addactivity(Activity activity, HttpServletRequest request) {
      HttpSession session = request.getSession();
      activity.setUid((Integer)session.getAttribute("role"));
      activity.setCreateime(new Date());
      activity.setRemarks(String.valueOf(0));
      this.activityService.addactivity(activity);
      return Result.success("活动申请中");
   }

   @GetMapping("/test")
   @ResponseBody
   public Result<Object> list(ActivityVo activityVo, @RequestParam("page") int page, @RequestParam("limit") int limit, HttpServletRequest request, Model model) {
      HttpSession session = request.getSession();
      Integer role = (Integer)session.getAttribute("role");
      String oid = (String)session.getAttribute("oid");
      IPage<Activity> iPage = this.activityService.Allactivity(activityVo, page, limit, role, oid);
      Long count = iPage.getTotal();
      model.addAttribute("list", iPage.getRecords());
      List<Activity> records = iPage.getRecords();
      Collections.reverse(records);
      return Result.success(records, count);
   }

   @GetMapping("/apply")
   @ResponseBody
   public Result<Object> application(ActivityVo activityVo, @RequestParam("page") int page, @RequestParam("limit") int limit, HttpServletRequest request, Model model) {
      HttpSession session = request.getSession();
      Integer role = (Integer)session.getAttribute("role");
      String oid = (String)session.getAttribute("oid");
      IPage<Activity> iPage = this.activityService.apply(activityVo, page, limit, role, oid);
      Long count = iPage.getTotal();
      model.addAttribute("list", iPage.getRecords());
      List<Activity> records = iPage.getRecords();
      Collections.reverse(records);
      return Result.success(records, count);
   }

   @GetMapping("/detail/{id}")
   public String detail(@PathVariable("id") Integer id, Model model) {
      Activity activity = this.activityService.selectActById(id);
      model.addAttribute("activity", activity);
      return "Activity/ActivityDetails";
   }

   @GetMapping("/edit/{id}")
   public String Edit(@PathVariable("id") Integer id, Model model) {
      Activity activity = this.activityService.selectActById(id);
      model.addAttribute("activity", activity);
      return "Activity/ActivityEdit";
   }

   @PutMapping("/update/{remarks}")
   @ResponseBody
   public Result<Object> Update(Activity activity, @PathVariable("remarks") String remarks) {
      if (remarks.equals("1")) {
         activity.setRemarks(remarks);
         this.activityService.update(activity);
         return Result.success("审批通过");
      } else {
         activity.setRemarks(remarks);
         this.activityService.update(activity);
         return Result.success("审批不通过");
      }
   }

   @PutMapping("/update/edit")
   @ResponseBody
   public Result<Object> EditUpdate(Activity activity) {
      this.activityService.update(activity);
      return Result.success("重新编辑成功");
   }

   @PutMapping("/delete")
   @ResponseBody
   public Result<Object> Delete(Activity activity) {
      this.activityService.delete(activity);
      return Result.success("取消成功");
   }
}
