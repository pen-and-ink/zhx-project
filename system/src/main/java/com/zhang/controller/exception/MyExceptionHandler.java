package com.zhang.controller.exception;

import com.zhang.util.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MyExceptionHandler {
   @ResponseBody
   @ExceptionHandler
   public Result<Object> myHandler(Exception e) {
      return Result.fail("系统错误" + e.getMessage());
   }
}
