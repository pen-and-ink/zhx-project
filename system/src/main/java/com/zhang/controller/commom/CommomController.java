package com.zhang.controller.commom;

import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class CommomController {
   @GetMapping("/towecomle")
   public String towecomle() {
      return "welcome";
   }

   @GetMapping("/")
   public String tologin() {
      return "login";
   }

   @RequestMapping("/captcha")
   public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
      CaptchaUtil.out(request, response);
   }
}
