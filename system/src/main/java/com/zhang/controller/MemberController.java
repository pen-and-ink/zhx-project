package com.zhang.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Member;
import com.zhang.pojo.Organ;
import com.zhang.service.MemberService;
import com.zhang.util.Result;
import com.zhang.vo.Memberkey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/member")
public class MemberController {
   @Autowired
   private MemberService memberService;

   @GetMapping("")
   public String list() {
      return "Member/memberlist";
   }

   @GetMapping("/retire")
   public String Retire() {
      return "Member/RetireList";
   }

   @GetMapping("/list")
   @ResponseBody
   public Result<Object> list(Memberkey memberkey, HttpServletRequest request, @RequestParam("page") int page, @RequestParam("limit") int limit) {
      HttpSession session = request.getSession();
      Integer role = (Integer)session.getAttribute("role");
      String useroid = (String)session.getAttribute("oid");
      IPage<Member> iPage = this.memberService.getAll(memberkey, page, limit, role, useroid, "1");
      Long count = iPage.getTotal();
      return Result.success(iPage.getRecords(), count);
   }

   @GetMapping("/retirelist")
   @ResponseBody
   public Result<Object> retirelist(Memberkey memberkey, HttpServletRequest request, @RequestParam("page") int page, @RequestParam("limit") int limit) {
      HttpSession session = request.getSession();
      Integer role = (Integer)session.getAttribute("role");
      String useroid = (String)session.getAttribute("oid");
      IPage<Member> iPage = this.memberService.getAll(memberkey, page, limit, role, useroid, "0");
      Long count = iPage.getTotal();
      return Result.success(iPage.getRecords(), count);
   }

   @DeleteMapping("/{ids}")
   @ResponseBody
   public Result<Object> del(@PathVariable("ids") String ids) {
      String[] s1 = ids.split(",");

      for(int i = 0; i < s1.length; ++i) {
         int wq = Integer.parseInt(s1[i]);
         this.memberService.delete(wq);
      }

      return Result.success("删除成功");
   }

   @PostMapping("/add")
   @ResponseBody
   public Result<Object> addMember(Member member) {
      this.memberService.addMember(member);
      return Result.success("新增成功");
   }

   @GetMapping("/add/ui")
   public String toaddUI(Model model) {
      List<Organ> organList = this.memberService.getAllDept();
      model.addAttribute("orginList", organList);
      return "Member/memberadd";
   }

   @GetMapping("/edit/{id}")
   public String getMemberId(@PathVariable("id") Integer id, Model model) {
      Member member = this.memberService.getMemById(id);
      model.addAttribute("member", member);
      model.addAttribute("oidlist", this.memberService.getAllDept());
      return "Member/memberEdit";
   }

   @PutMapping("/updateMember")
   @ResponseBody
   public Result<Object> update(Member member) {
      this.memberService.updateMember(member);
      return Result.success("员工信息修改成功!");
   }

   @PutMapping("/retire/{id}")
   @ResponseBody
   public Result<Object> ret(@PathVariable("id") String id) {
      String[] s1 = id.split(",");

      for(int i = 0; i < s1.length; ++i) {
         int wq = Integer.parseInt(s1[i]);
         this.memberService.retire(id);
      }

      return Result.success("退休成功");
   }
}
