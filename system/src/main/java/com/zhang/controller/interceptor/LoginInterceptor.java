package com.zhang.controller.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginInterceptor implements HandlerInterceptor {
   private static final Logger log = LoggerFactory.getLogger(LoginInterceptor.class);

   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      Object user = request.getSession().getAttribute("userInfo");
      if (user == null) {
         log.debug("未登录" + request.getRequestURI());
         response.sendRedirect("/");
         return false;
      } else {
         log.debug("放行请求：" + request.getRequestURI());
         return true;
      }
   }
}
