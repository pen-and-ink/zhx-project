package com.zhang.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebConfig implements WebMvcConfigurer {
   @Autowired
   @Qualifier("loginInterceptor")
   private HandlerInterceptor handlerInterceptor;

   public void addInterceptors(InterceptorRegistry registry) {
      InterceptorRegistration registration = registry.addInterceptor(this.handlerInterceptor);
      registration.addPathPatterns("/**");
      registration.excludePathPatterns("/", "/api/**",
              "/js/**", "/css/**", "/images/**", "/lib/**", "/webjars/**", "/captcha", "/login");
   }
}
