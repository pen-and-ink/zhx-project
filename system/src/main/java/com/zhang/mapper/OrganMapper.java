package com.zhang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhang.pojo.Organ;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrganMapper extends BaseMapper<Organ> {
   @Select({"select * from s_organ_details where student_id=#{studentId}"})
   List<Organ> selectByStudentId(Integer studentId);
}
