package com.zhang.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Member;
import com.zhang.pojo.Organ;
import com.zhang.vo.Memberkey;

import java.util.List;

public interface MemberService {
   IPage<Member> getAll(Memberkey memberkey, int limit, int limits, Integer role, String useroid, String retire);

   Integer delete(Integer id);

   void addMember(Member member);

   List<Organ> getAllDept();

   Member getMemById(Integer id);

   void updateMember(Member member);

   void retire(String id);
}
