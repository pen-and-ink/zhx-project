package com.zhang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.mapper.ActivityMapper;
import com.zhang.pojo.Activity;
import com.zhang.service.ActivityService;
import com.zhang.vo.ActivityVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {
   @Autowired
   private ActivityMapper activityMapper;

   public Integer addactivity(Activity activity) {
      return this.activityMapper.insert(activity);
   }

   public void update(Activity activity) {
      this.activityMapper.updateById(activity);
   }

   public List<Activity> selcteActivity(Integer uid) {
      QueryWrapper<Activity> wrapper = new QueryWrapper<>();
      wrapper.ne("remarks", "1");
      if (uid == 1) {
         List<Activity> list = activityMapper.selectList(wrapper);
         return list;
      } else {
         wrapper.eq("uid", uid);
         List<Activity> list= activityMapper.selectList(wrapper);
         return list;
      }
   }

   public Integer EditActById(Activity activity) {
      int i = this.activityMapper.updateById(activity);
      return i;
   }

   public Activity selectActById(Integer id) {
      return activityMapper.selectById(id);
   }

   public IPage<Activity> Allactivity(ActivityVo activityVo, int limit, int limits, Integer role, String oid) {
      QueryWrapper<Activity> wrapper = new QueryWrapper<>();
      wrapper.like(StringUtils.isNoneBlank(activityVo.getActivity()), "activity", activityVo.getActivity());
      wrapper.like(StringUtils.isNoneBlank(activityVo.getAssname()), "assname", activityVo.getAssname());
      wrapper.ne("remarks", "0");
      if (role == 1) {
         Page<Activity> page = new Page<>(limit, limits);
         return activityMapper.selectPage(page, wrapper);
      } else {
         wrapper.eq("uid", role);
         wrapper.eq("assname", oid);
         Page<Activity> page = new Page<>(limit, limits);
         return activityMapper.selectPage(page, wrapper);
      }
   }

   public IPage<Activity> apply(ActivityVo activityVo, int limit, int limits, Integer role, String oid) {
      QueryWrapper<Activity> wrapper = new QueryWrapper<>();
      wrapper.like(StringUtils.isNoneBlank(activityVo.getActivity()), "activity", activityVo.getActivity());
      wrapper.like(StringUtils.isNoneBlank(activityVo.getAssname()), "assname", activityVo.getAssname());
      wrapper.eq("remarks", "0");
      if (role == 1) {
         Page<Activity> page = new Page<>(limit, limits);
         return activityMapper.selectPage(page, wrapper);
      } else {
         wrapper.eq("uid", role);
         wrapper.eq("assname", oid);
         Page<Activity> page = new Page<>(limit, limits);
         return activityMapper.selectPage(page, wrapper);
      }
   }

   public void delete(Activity activity) {
      this.activityMapper.deleteById(activity.getId());
   }
}
