package com.zhang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.mapper.OrganMapper;
import com.zhang.pojo.Organ;
import com.zhang.service.OragnService;
import com.zhang.vo.OrginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganServiceImpl implements OragnService {
   @Autowired
   private OrganMapper organMapper;

   public Page<Organ> getAllDepartment(int Startpage, int pageSize) {
      Page<Organ> page = new Page<>(Startpage,pageSize);
      this.organMapper.selectPage(page, null);
      return page;
   }

   public Integer delete(Integer id) {
      return organMapper.deleteById(id);
   }

   public IPage<Organ> getAllOragn(OrginVo orginVo, int limit, int limits) {
      String name = orginVo.getName();
      String president = orginVo.getPresident();
      String college = orginVo.getCollege();
      QueryWrapper<Organ> wrapper = new QueryWrapper<>();
      wrapper.like(StringUtils.isNoneBlank(name), "name", name);
      wrapper.like(StringUtils.isNoneBlank(president), "President", president);
      wrapper.like(StringUtils.isNoneBlank(college), "college", college);
      Page<Organ> page = new Page<>(limit, limits);
      return organMapper.selectPage(page, wrapper);
   }

   public List<Organ> getAllDept() {
      List<Organ> list = organMapper.selectList(null);
      return list;
   }

   public Organ getOne(Integer id) {
      return organMapper.selectById(id);
   }

   public void addOragn(Organ organ) {
      organMapper.insert(organ);
   }

   public void Update(Organ organ) {
      QueryWrapper<Organ> wrapper = new QueryWrapper<>();
      wrapper.eq("id", organ.getId());
      organMapper.update(organ, wrapper);
   }
}
