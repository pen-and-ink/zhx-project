package com.zhang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.mapper.MemberMapper;
import com.zhang.mapper.OrganMapper;
import com.zhang.pojo.Member;
import com.zhang.pojo.Organ;
import com.zhang.service.MemberService;
import com.zhang.vo.Memberkey;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberSeriveImpl implements MemberService {
   @Autowired
   private MemberMapper memberMapper;
   @Autowired
   private OrganMapper organMapper;

   public IPage<Member> getAll(Memberkey memberkey, int limit, int limits, Integer role, String useroid, String retire) {
      String name = memberkey.getName();
      String oid = memberkey.getOid();
      String sno = memberkey.getSno();
      QueryWrapper<Member> wrapper = new QueryWrapper<>();
      wrapper.like(StringUtils.isNoneBlank(name), "name", name);
      wrapper.like(StringUtils.isNoneBlank(oid), "oid", oid);
      wrapper.like(StringUtils.isNoneBlank(sno), "sno", sno);
      wrapper.eq("retire", retire);

      if (role == 1) {
         Page<Member> page = new Page<>(limit, limits);
         return this.memberMapper.selectPage(page, wrapper);
      } else {
         wrapper.eq("oid", useroid);
         Page<Member> page = new Page<>(limit, limits);
         return this.memberMapper.selectPage(page, wrapper);
      }
   }

   public Integer delete(Integer id) {
      return memberMapper.deleteById(id);
   }

   public void addMember(Member member) {
      memberMapper.insert(member);
   }

   public List<Organ> getAllDept() {
      return organMapper.selectList(null);
   }

   public Member getMemById(Integer id) {
      return this.memberMapper.selectById(id);
   }

   public void updateMember(Member member) {
      memberMapper.updateById(member);
   }

   public void retire(String id) {
      Member member = memberMapper.selectById(id);
      member.setRetire("0");
      memberMapper.updateById(member);
   }
}
