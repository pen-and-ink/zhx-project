package com.zhang.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.mapper.AccountMapper;
import com.zhang.pojo.Account;
import com.zhang.service.AccountService;
import com.zhang.vo.AccountVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
   @Autowired
   private AccountMapper accountMapper;

   public Account login(String account, String password) {
      QueryWrapper<Account> wrapper = new QueryWrapper<>();
      wrapper.eq("account", account).eq("password", password);
      return accountMapper.selectOne(wrapper);
   }

   public void update(Account account) {
      this.accountMapper.updateById(account);
   }

   public IPage<Account> getAllAccount(AccountVo accountVo, int limit, int limits) {
      String name = accountVo.getName();
      String account = accountVo.getAccount();
      String oid = accountVo.getOid();
      QueryWrapper<Account> wrapper =  new QueryWrapper<>();
      wrapper.like(StringUtils.isNoneBlank(name), "name", name);
      wrapper.like(StringUtils.isNoneBlank(account), "account", account);
      wrapper.like(StringUtils.isNoneBlank(oid), "oid", oid);
      Page<Account> page = new Page<>(limit, limits);
      return accountMapper.selectPage(page, wrapper);
   }

   public Integer DelAccountById(Integer id) {
      int i = accountMapper.deleteById(id);
      return i;
   }

   public Integer AddAccount(Account account) {
      String s = account.getAccount();
      QueryWrapper<Account> wrapper = new QueryWrapper<>();
      wrapper.eq("account", s);
      return accountMapper.selectOne(wrapper) == null ? this.accountMapper.insert(account) : -1;
   }

   public Account getOne(Integer id) {
      return accountMapper.selectById(id);
   }
}
