package com.zhang.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Account;
import com.zhang.vo.AccountVo;

public interface AccountService {
   Account login(String account, String password);

   void update(Account account);

   IPage<Account> getAllAccount(AccountVo accountVo, int limit, int limits);

   Integer DelAccountById(Integer id);

   Integer AddAccount(Account account);

   Account getOne(Integer id);
}
