package com.zhang.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhang.pojo.Organ;
import com.zhang.vo.OrginVo;

import java.util.List;

public interface OragnService {
   Page<Organ> getAllDepartment(int Startpage, int pageSize);

   Integer delete(Integer id);

   IPage<Organ> getAllOragn(OrginVo orginVo, int limit, int limits);

   List<Organ> getAllDept();

   Organ getOne(Integer id);

   void addOragn(Organ organ);

   void Update(Organ organ);
}
