package com.zhang.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhang.pojo.Activity;
import com.zhang.vo.ActivityVo;

import java.util.List;

public interface ActivityService {
   Integer addactivity(Activity activity);

   void update(Activity activity);

   List<Activity> selcteActivity(Integer uid);

   Integer EditActById(Activity activity);

   Activity selectActById(Integer id);

   IPage<Activity> Allactivity(ActivityVo activityVo, int limit, int limits, Integer role, String oid);

   IPage<Activity> apply(ActivityVo activityVo, int limit, int limits, Integer role, String oid);

   void delete(Activity activity);
}
