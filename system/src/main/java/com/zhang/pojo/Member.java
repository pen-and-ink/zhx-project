package com.zhang.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member {
   @TableId(type = IdType.AUTO)
   private Integer id;
   private String sno;
   private String name;
   private String sex;
   private String phone;
   private String qq;
   private String grade;
   private String major;
   private String oid;
   private String identity;
   private String jointime;
   private String depart;
   private String retire;

}
