package com.zhang.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Organ {
   @TableId
   private int id;
   private String name;
   private String createDate;
   private String phone;
   private String level;
   private String details;
   private String President;
   private String teacher;
   private String college;

}
