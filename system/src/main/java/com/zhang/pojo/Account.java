package com.zhang.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("s_sys_user")
public class Account {
   @TableId(type = IdType.AUTO)
   private int id;
   private int role;
   private String sno;
   private Date createTime;
   private Date modifyTime;
   private String name;
   private String account;
   private String password;
   private String phone;
   private String email;
   private int sex;
   private String oid;

}