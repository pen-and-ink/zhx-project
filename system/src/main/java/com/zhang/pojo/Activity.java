package com.zhang.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("activity")
public class Activity {
   @TableId(type = IdType.AUTO)
   private Integer id;
   private String activity;
   private Integer uid;
   private String association;
   private String assname;
   private String name;
   private String sno;
   private String phone;
   private String teacher;
   private String place;
   private String Startdate;
   private String Enddate;
   private String Endtime;
   private String Starttime;
   private Integer level;
   private Integer people;
   private Integer money;
   private String remarks;
   private Date createime;
   private String area;
   private String notes;
   private String detail;



}
