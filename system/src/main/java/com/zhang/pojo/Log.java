package com.zhang.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("s_sys_log")
public class Log {
   @TableId
   private long id;
   private Date createTime;
   private String ip;
   private String logname;
   private String message;
   private String succeed;
   private String account;

}
