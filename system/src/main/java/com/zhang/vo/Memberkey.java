package com.zhang.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Memberkey {
   private String name;
   private String sno;
   private String oid;

}
