package com.zhang.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data

@AllArgsConstructor
public class Result<T> {
   private Integer code;
   private String message;
   private T data;
   private Long count;

   private Result() {
   }

   public static Result<Object> success() {
      return new Result(0, "success", (Object)null, (Long)null);
   }

   public static Result<Object> success(String message) {
      return new Result(0, message, (Object)null, (Long)null);
   }

   public static Result<Object> success(Object data, Long count) {
      return new Result(0, "success", data, count);
   }

   public static Result<Object> fail() {
      return new Result(-1, "fail", (Object)null, (Long)null);
   }

   public static Result<Object> fail(String message) {
      return new Result(-1, message, (Object)null, (Long)null);
   }


}
