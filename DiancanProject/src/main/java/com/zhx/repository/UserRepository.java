package com.zhx.repository;

import com.zhx.pojo.UserInfo;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<UserInfo, String> {
    UserInfo findByOpenid(String openid);
}
