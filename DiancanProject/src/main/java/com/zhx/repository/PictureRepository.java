package com.zhx.repository;

import com.zhx.pojo.PictureInfo;

import org.springframework.data.jpa.repository.JpaRepository;


public interface PictureRepository extends JpaRepository<PictureInfo, Integer> {
    PictureInfo findByPicId(Integer picId);
}
