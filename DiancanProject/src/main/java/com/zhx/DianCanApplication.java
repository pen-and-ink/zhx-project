package com.zhx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DianCanApplication {

	public static void main(String[] args) {

		SpringApplication.run(DianCanApplication.class, args);
		System.out.println("http://localhost:8080/diancan/leimu/list");
	}
}
