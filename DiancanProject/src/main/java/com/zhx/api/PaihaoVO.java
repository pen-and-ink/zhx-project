package com.zhx.api;

import lombok.Data;


@Data
public class PaihaoVO {
    private Integer num;
    private Integer type;
    private Integer smallOkNum;//小桌当前就位号码
    private Integer bigOkNum;//大桌当前就位号码
}
