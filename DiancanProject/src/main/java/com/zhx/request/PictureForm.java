package com.zhx.request;

import lombok.Data;


@Data
public class PictureForm {

    private Integer picId;
    private String picUrl;
    private String picMessage;
}
