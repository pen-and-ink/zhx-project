package com.zhx.controller;

import com.zhx.pojo.Comment;
import com.zhx.repository.CommentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/comment")
@Slf4j
public class AdminCommentController {

    @Autowired
    private CommentRepository commentRepository;


    //列表
    @GetMapping("/list")
    public String list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size,
                       ModelMap map) {
        PageRequest request = PageRequest.of(page - 1, size);
        Page<Comment> commentPage = commentRepository.findAll(request);
        map.put("commentPage", commentPage);
        map.put("currentPage", page);
        map.put("size", size);
        return "comment/list";
    }


}
